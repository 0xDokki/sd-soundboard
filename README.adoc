= 0xDiddis Streamdeck Soundboard

This is my take on a soundboard plugin for
the Elgato Stream Deck.

I created it mainly because the default soundboard plugin
doesn't allow you to choose an output device,
and when I tried to set it to anything else than the
default device in the windows sound settings, it wouldn't
play at all.

== Usage

When first added to the deck, the tile will show a red box.
That box is a status indicator, and red means error.
The only error currently indicated is when no file
was selected, or the selected file doesn't exist.

When you select a file, you might see a quick blip
of orange, which means the file is being loaded and prepared.
You'll also see that loading stage when (re-) starting the
Stream Deck application, or after opening a folder.

For me, loading a folder full of soundboard actions
between a few seconds and some minutes each takes about
2 to 3 seconds until the last tile is ready to be played.
I assume that most of that is caused by the Stream Deck
application delaying starts, as loading itself only takes
on the order of a few dozen milliseconds each.

When loaded, the box will go green and pressing the button
will start or stop the playback. While playing, the box
will turn a light blue color.

== Remarks

* The javascript code for the property inspector
  is a bit of a mess but for now it works.
  I might revisit it later.

== Support

Currently, this plugin only works on windows, as audio
support in dotnet core seems quite limited.
I'm currently using NAudio, which I heard wouldn't work
either, but it does (on my machine at least).

Also, the windows API I'm using to read out the device names
(`waveOutGetDevCaps` in winmm.dll) cuts off after 32 characters.
