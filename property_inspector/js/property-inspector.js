﻿// global websocket, used to communicate from/to Stream Deck software
// as well as some info about our plugin, as sent by Stream Deck software 
let websocket = null,
    uuid = null,
    inInfo = null,
    actionInfo = {},
    settingsModel = {
        Volume: 0,
        File: null,
        Device: -1,
        Devices: {},
        UpdateDevices: false,
    };

function connectElgatoStreamDeckSocket(inPort, inUUID, inRegisterEvent, inInfo, inActionInfo) {
    uuid = inUUID;
    actionInfo = JSON.parse(inActionInfo);
    websocket = new WebSocket('ws://localhost:' + inPort);

    //initialize values
    if (actionInfo.payload.settings.settingsModel) {
        settingsModel.Volume = actionInfo.payload.settings.settingsModel.Volume;
        settingsModel.File = actionInfo.payload.settings.settingsModel.File;
        settingsModel.Device = actionInfo.payload.settings.settingsModel.Device + '';
        settingsModel.Devices = actionInfo.payload.settings.settingsModel.Devices;
        settingsModel.UpdateDevices = actionInfo.payload.settings.settingsModel.UpdateDevices;
    }
    
    initView();

    websocket.onopen = function () {
        let json = {event: inRegisterEvent, uuid: inUUID};
        // register property inspector to Stream Deck
        websocket.send(JSON.stringify(json));
    };

    websocket.onmessage = function (evt) {
        // Received message from Stream Deck
        let jsonObj = JSON.parse(evt.data);
        let sdEvent = jsonObj['event'];
        switch (sdEvent) {
            case "didReceiveSettings":
                if (jsonObj.payload.settings.settingsModel) {
                    settingsModel.Volume = jsonObj.payload.settings.settingsModel.Volume;
                    settingsModel.File = jsonObj.payload.settings.settingsModel.File;
                    settingsModel.Device = jsonObj.payload.settings.settingsModel.Device + '';
                    settingsModel.Devices = jsonObj.payload.settings.settingsModel.Devices;
                    settingsModel.UpdateDevices = jsonObj.payload.settings.settingsModel.UpdateDevices;
                    
                    initView(true);
                }
                break;
            default:
                break;
        }
    };
}

const initView = (clear) => {
    document.getElementById('volumeRange').value = settingsModel.Volume * 100;

    if (settingsModel.File) {
        document.getElementById('fileDisplay').textContent = settingsModel.File.length > 28
            ? settingsModel.File.substr(settingsModel.File.lastIndexOf('/') + 1, settingsModel.File.length)
            : settingsModel.File;
    }

    const deviceList = document.getElementById('deviceList');
    if (clear) while (deviceList.hasChildNodes()) deviceList.removeChild(deviceList.firstChild);

    let liClass = (settingsModel.Device === '-1') ? 'selected' : '';
    deviceList.appendChild(toElement(`<li key="-1" onclick="setDevice(this);" class="${liClass}">Default</li>`));
    if (settingsModel.Device === '-1') prevSelected = deviceList.lastChild;

    for (let key in settingsModel.Devices) {
        const val = settingsModel.Devices[key];
        liClass = (settingsModel.Device === key) ? 'selected' : '';
        deviceList.appendChild(toElement(`<li key="${key}" onclick="setDevice(this);" class="${liClass}">${val}</li>`));
        if (settingsModel.Device === key) prevSelected = deviceList.lastChild;
    }
};

let prevSelected = null;
const setDevice = (element) => {
    if (prevSelected) prevSelected.classList.remove('selected');
    prevSelected = element;
    element.classList.add('selected');

    setSettings(element.getAttribute('key'), "Device");
};

const setSettings = (value, param) => {
    if (websocket) {
        if (param === 'File') {
            value = decodeURIComponent(value.substring(12));
            document.getElementById('fileDisplay').textContent = value.length > 28
                ? value.substr(value.lastIndexOf('/') + 1, value.length)
                : value;
        }

        settingsModel[param] = value;
        let json = {
            "event": "setSettings",
            "context": uuid,
            "payload": {
                "settingsModel": settingsModel
            }
        };
        websocket.send(JSON.stringify(json));
    }
};

const template = document.createElement('template');
const toElement = (str) => {
    if (template.firstChild)
        template.removeChild(template.firstChild);
    str = str.trim();
    template.innerHTML = str;
    return template.content.firstChild;
};


