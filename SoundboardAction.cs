﻿using System.IO;
using System.Threading.Tasks;
using NAudio.Wave;
using sd_soundboard.Models;
using StreamDeckLib;
using StreamDeckLib.Messages;

namespace sd_soundboard {
    [ActionUuid(Uuid = "diddi.soundboard.action")]
    public class SoundboardAction : BaseStreamDeckActionWithSettingsModel<SoundBoardSettings> {
        private AudioFileReader _reader;
        private WaveOutEvent _player;

        public override async Task OnKeyUp(StreamDeckEventPayload args) {
            if (_reader == null) {
                await Manager.ShowAlertAsync(args.context);
                return;
            }

            var wasPlaying = _player.PlaybackState == PlaybackState.Playing;
            _player.Stop();
            _reader.Position = 0;
            if (!wasPlaying) {
                _player.Play();
                await Manager.SetImageAsync(args.context, "images/icon_info.png");
            } else await Manager.SetImageAsync(args.context, "images/icon_ok.png");
        }

        public override async Task OnDidReceiveSettings(StreamDeckEventPayload args) {
            await base.OnDidReceiveSettings(args);

            _player.Stop();
            _reader?.Dispose();

            await UpdateSettings(args.context);
        }

        public override async Task OnWillAppear(StreamDeckEventPayload args) {
            await base.OnWillAppear(args);
            _player = new WaveOutEvent();
            _player.PlaybackStopped += async (s, e) => await Manager.SetImageAsync(args.context, "images/icon_ok.png");

            await UpdateSettings(args.context);

            await Manager.SetSettingsAsync(args.context, SettingsModel);
        }

        private async Task UpdateSettings(string ctx) {
            if (SettingsModel.UpdateDevices) {
                SettingsModel.UpdateDevices = false;

                SettingsModel.Device = -1;
                SettingsModel.Devices.Clear();
                
                var caps = DeviceHelper.GetAudioDevices();
                for (var i = 0; i < caps.Count; i++) SettingsModel.Devices.Add(i, caps[i].ProductName);
                
                await Manager.SetSettingsAsync(ctx, SettingsModel);
            }
            
            if (File.Exists(SettingsModel.File)) {
                await Manager.SetImageAsync(ctx, "images/icon_warning.png");

                _reader = new AudioFileReader(SettingsModel.File);

                _player.Volume = SettingsModel.Volume;
                _player.DeviceNumber = SettingsModel.Device;

                _player.Init(_reader);

                await Manager.SetImageAsync(ctx, "images/icon_ok.png");
            } else
                await Manager.SetImageAsync(ctx, "images/icon_error.png");
        }

        public override async Task OnWillDisappear(StreamDeckEventPayload args) {
            await base.OnWillDisappear(args);

            _player.Dispose();
            _reader.Dispose();
        }
    }
}